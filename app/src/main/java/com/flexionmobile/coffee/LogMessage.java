package com.flexionmobile.coffee;

import java.io.Serializable;
import java.util.Date;

public class LogMessage implements Serializable {

    private Date created;
    private String message;

    public LogMessage(Date created, String message) {
        this.created = created;
        this.message = message;
    }

    public LogMessage() {

    }

    public Date getCreated() {
        return created;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogMessage that = (LogMessage) o;

        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        return message != null ? message.equals(that.message) : that.message == null;

    }

    @Override
    public int hashCode() {
        int result = created != null ? created.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}

