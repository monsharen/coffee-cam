package com.flexionmobile.coffee;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Photo implements Serializable {

    private String id;
    private Date created;
    private byte[] data;

    public Photo(Date created, byte[] data) {
        this.created = created;
        this.data = data;
        id = UUID.randomUUID().toString();
    }

    public Date getCreated() {
        return created;
    }

    public byte[] getData() {
        return data;
    }

    public String getId() {
        return id;
    }
}
