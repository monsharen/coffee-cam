package com.flexionmobile.coffee;

import java.io.Serializable;

public class IdentifiedArea implements Serializable {

    private final int startX;
    private final int endX;
    private final int startY;
    private final int endY;

    public IdentifiedArea(int startX, int endX, int startY, int endY) {
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
    }

    public int getStartX() {
        return startX;
    }

    public int getEndX() {
        return endX;
    }

    public int getStartY() {
        return startY;
    }

    public int getEndY() {
        return endY;
    }

    @Override
    public String toString() {
        return "IdentifiedArea{" +
                "startX=" + startX +
                ", endX=" + endX +
                ", startY=" + startY +
                ", endY=" + endY +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentifiedArea that = (IdentifiedArea) o;

        if (startX != that.startX) return false;
        if (endX != that.endX) return false;
        if (startY != that.startY) return false;
        return endY == that.endY;

    }

    @Override
    public int hashCode() {
        int result = startX;
        result = 31 * result + endX;
        result = 31 * result + startY;
        result = 31 * result + endY;
        return result;
    }
}
