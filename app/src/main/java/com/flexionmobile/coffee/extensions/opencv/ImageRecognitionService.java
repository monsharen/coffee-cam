package com.flexionmobile.coffee.extensions.opencv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;
import com.flexionmobile.coffee.IdentifiedArea;
import com.flexionmobile.coffee.coffeecam.R;
import com.flexionmobile.coffee.events.ImageRecognitionEvent;
import com.flexionmobile.coffee.events.PhotoTakenEvent;
import com.flexionmobile.coffee.services.CoffeeService;
import com.flexionmobile.coffee.util.Util;
import com.flexionmobile.coffee.events.Events;
import org.opencv.android.InstallCallbackInterface;
import org.opencv.android.LoaderCallbackInterface;

public class ImageRecognitionService extends CoffeeService {

    private static final String TAG = "ImageRecognitionService";

    private BroadcastReceiver broadcastReceiver;
    private OpenCv openCv;

    @Override
    public void onCreate() {
        super.onCreate();

        initiateOpenCv();
    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private void initiateOpenCv() {
        Bitmap templateImage = getTemplateImage(this);

        openCv = new OpenCv(templateImage, true);
        openCv.initialise(this, new LoaderCallbackInterface() {
            @Override
            public void onManagerConnected(int i) {
                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {

                        Parcelable event = intent.getParcelableExtra(Events.EVENT);
                        if (event instanceof PhotoTakenEvent) {
                            PhotoTakenEvent photoTakenEvent = (PhotoTakenEvent) event;
                            byte[] imageBytes = photoTakenEvent.getImageBytes();

                            OpenCvResult analyse;
                            try {
                                analyse = openCv.analyse(imageBytes);
                            } catch (NoIdentifiedAreaException e) {
                                Log.e(TAG, "failed to find template in photo");
                                return;
                            }
                            IdentifiedArea identifiedArea = analyse.getIdentifiedArea();
                            Bitmap visual = analyse.getVisual();
                            byte[] bytes = Util.toBytes(visual);
                            ImageRecognitionEvent imageRecognitionEvent = new ImageRecognitionEvent(bytes, identifiedArea);
                            sendEvent(imageRecognitionEvent);
                        }
                    }
                };
                localBroadcastManager.registerReceiver((broadcastReceiver), new IntentFilter(Events.RESULT));
            }

            @Override
            public void onPackageInstall(int i, InstallCallbackInterface installCallbackInterface) {
                Toast.makeText(ImageRecognitionService.this, "Error! Make sure you have installed OpenCV Manager from Google Play", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Bitmap getTemplateImage(Context context) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.outHeight = 640;
        options.outWidth = 480;
        Bitmap template = BitmapFactory.decodeResource(context.getResources(), R.drawable.cut_out_template, options);
        return Bitmap.createScaledBitmap(template, 253, 294, false);
    }
}
