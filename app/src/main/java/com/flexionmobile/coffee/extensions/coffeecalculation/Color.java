package com.flexionmobile.coffee.extensions.coffeecalculation;

class Color {

    private final int red;
    private final int green;
    private final int blue;

    Color(int argb) {
        this.red = android.graphics.Color.red(argb);
        this.green = android.graphics.Color.green(argb);
        this.blue = android.graphics.Color.blue(argb);
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }
}
