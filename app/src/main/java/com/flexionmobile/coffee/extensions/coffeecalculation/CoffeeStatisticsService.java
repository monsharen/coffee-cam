package com.flexionmobile.coffee.extensions.coffeecalculation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.util.Log;
import com.flexionmobile.coffee.events.CoffeeStatisticsEvent;
import com.flexionmobile.coffee.events.MotionDetectionEndedEvent;
import com.flexionmobile.coffee.services.CoffeeService;
import com.flexionmobile.coffee.util.Util;
import com.flexionmobile.coffee.events.Events;
import com.flexionmobile.coffee.IdentifiedArea;

public class CoffeeStatisticsService extends CoffeeService {

    private static final String TAG = "CoffeeStatisticsService";

    private BroadcastReceiver broadcastReceiver;
    private Gustav gustav;

    @Override
    public void onCreate() {
        super.onCreate();

        gustav = new TopDownGustav();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Parcelable event = intent.getParcelableExtra(Events.EVENT);
                if (event instanceof MotionDetectionEndedEvent) {
                    MotionDetectionEndedEvent motionDetectionEndedEvent = (MotionDetectionEndedEvent) event;
                    byte[] imageBytes = motionDetectionEndedEvent.getImageBytes();

                    Bitmap originalImage = Util.toBitmap(imageBytes);
                    Bitmap croppedImage = getCroppedImage(motionDetectionEndedEvent.getIdentifiedArea(), originalImage);
                    GustavResult gustavResult = gustav.analyse(croppedImage);

                    CoffeeStatisticsEvent coffeeStatisticsEvent = new CoffeeStatisticsEvent(gustavResult.getAmountOfCoffeeLeftInPercentage());
                    sendEvent(coffeeStatisticsEvent);
                    //String percentage = Util.toPercentage(gustavResult.getAmountOfCoffeeLeftInPercentage());
                    //LogMessage logMessage = new LogMessage(new Date(), "coffee left: " + percentage);
                    //LogMessageEvent logMessageEvent = new LogMessageEvent(logMessage);
                    //sendEvent(logMessageEvent);
                }
            }
        };
        localBroadcastManager.registerReceiver((broadcastReceiver), new IntentFilter(Events.RESULT));
    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private Bitmap getCroppedImage(IdentifiedArea identifiedArea, Bitmap bitmap) {
        Log.d(TAG, "Cropping image for area " + identifiedArea);

        int width = identifiedArea.getEndX() - identifiedArea.getStartX();
        int height = identifiedArea.getEndY() - identifiedArea.getStartY();

        Log.d(TAG, "Cropping image from x=" + identifiedArea.getStartX() + ", y=" + identifiedArea.getStartY() + ", width=" + width + ", height=" + height);

        return Bitmap.createBitmap(bitmap, identifiedArea.getStartX(), identifiedArea.getStartY(), width, height);
    }
}
