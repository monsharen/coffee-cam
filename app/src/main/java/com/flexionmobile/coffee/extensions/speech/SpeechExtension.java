package com.flexionmobile.coffee.extensions.speech;

import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import com.flexionmobile.coffee.extensionapi.service.AsyncServiceExtension;

import java.util.List;

public class SpeechExtension implements AsyncServiceExtension {

    private static final int SPEECH_SUPPORTED_ID = 12345;

    private Context context;
    @Override
    public void initialise(Context context) {
        this.context = context;
    }

    @Override
    public void dispose() {

    }

    @Override
    public List<String> getPermissions() {
        return null;
    }

    @Override
    public Intent getServiceIntent(Context context) {
        return new Intent(context, SpeechService.class);
    }

    @Override
    public Intent getActivityIntent() {
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        return checkIntent;
    }

    @Override
    public int getRequestCode() {
        return SPEECH_SUPPORTED_ID;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SPEECH_SUPPORTED_ID) {

            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                Intent intent = new Intent(context, SpeechService.class);
                context.startService(intent);
            } else {
                // missing data, install it
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                context.startActivity(installIntent);
            }
        }
    }
}
