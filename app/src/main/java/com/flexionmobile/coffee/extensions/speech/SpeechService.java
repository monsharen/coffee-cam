package com.flexionmobile.coffee.extensions.speech;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Parcelable;
import android.speech.tts.TextToSpeech;
import com.flexionmobile.coffee.LogMessage;
import com.flexionmobile.coffee.events.ChatEvent;
import com.flexionmobile.coffee.events.LogMessageEvent;
import com.flexionmobile.coffee.events.Events;
import com.flexionmobile.coffee.services.CoffeeService;

import java.util.*;


public class SpeechService extends CoffeeService {

    private static final String TAG = "SpeechService";

    private TextToSpeech mTts;

    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        mTts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                mTts.setLanguage(Locale.US);
                sendMessage(TAG, "text to speech initialised");

            }
        });

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Parcelable event = intent.getParcelableExtra(Events.EVENT);
                if (event instanceof ChatEvent) {
                    ChatEvent chatEvent = (ChatEvent) event;
                    say(chatEvent.getMessage());
                }
            }
        };
        localBroadcastManager.registerReceiver((broadcastReceiver), new IntentFilter(Events.RESULT));
    }

    @Override
    public void onDestroy() {
        mTts = null;
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private void say(String message) {
        log(message);
        mTts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    private void log(String message) {
        LogMessage logMessage = new LogMessage(new Date(), message);
        LogMessageEvent logMessageEvent = new LogMessageEvent(logMessage);
        sendEvent(logMessageEvent);
    }

}
