package com.flexionmobile.coffee.extensions.opencv;

import android.graphics.Bitmap;
import com.flexionmobile.coffee.IdentifiedArea;

public class OpenCvResult {

    private final IdentifiedArea identifiedArea;
    private final Bitmap visual;

    public OpenCvResult(IdentifiedArea identifiedArea, Bitmap visual) {
        this.identifiedArea = identifiedArea;
        this.visual = visual;
    }

    public IdentifiedArea getIdentifiedArea() {
        return identifiedArea;
    }

    public Bitmap getVisual() {
        return visual;
    }
}
