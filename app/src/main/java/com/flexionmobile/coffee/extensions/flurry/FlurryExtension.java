package com.flexionmobile.coffee.extensions.flurry;

import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import com.flexionmobile.coffee.events.Events;
import com.flexionmobile.coffee.extensionapi.Extension;
import com.flexionmobile.coffee.extensionapi.storage.StorageAware;
import com.flexionmobile.coffee.extensionapi.storage.StorageValue;
import com.flexionmobile.coffee.extensionapi.storage.ValueType;
import com.flexionmobile.coffee.storage.StorageService;
import com.flurry.android.FlurryAgent;

import java.util.*;

public class FlurryExtension implements Extension, StorageAware {

    private static final String DEFAULT_PROJECT_KEY = "W4WP2FQVSZFY6B3684X7";

    private LocalBroadcastManager localBroadcastManager;
    private FlurryBroadcastReceiver flurryBroadcastReceiver;
    private StorageService storageService;


    @Override
    public void initialise(Context context) {
        String projectKey = storageService.getString("project-key", DEFAULT_PROJECT_KEY);
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .withListener(new FlurryListener())
                .build(context, projectKey);
        flurryBroadcastReceiver = new FlurryBroadcastReceiver();
        localBroadcastManager = LocalBroadcastManager.getInstance(context);
        localBroadcastManager.registerReceiver(flurryBroadcastReceiver, new IntentFilter(Events.RESULT));
    }

    @Override
    public void dispose() {
        localBroadcastManager.unregisterReceiver(flurryBroadcastReceiver);
    }

    @Override
    public List<String> getPermissions() {
        return null;
    }

    @Override
    public String getId() {
        return "FlurryExtension";
    }

    @Override
    public void acceptStorageService(StorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public Map<String, ValueType> getSupportedStorageValues() {
        Map<String, ValueType> storageValues = new HashMap<>();
        storageValues.put("project-key", ValueType.STRING);
        return storageValues;
    }
}
