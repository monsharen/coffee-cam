package com.flexionmobile.coffee.extensions.opencv;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.flexionmobile.coffee.IdentifiedArea;
import com.flexionmobile.coffee.util.Util;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

public class OpenCv {

    private static final String TAG = "OpenCv";

    private final Bitmap template;
    private final boolean drawResult;

    private static final double CONFIDENCE_THRESHOLD = 100;

    public OpenCv(Bitmap template, boolean drawResult) {
        this.template = template;
        this.drawResult = drawResult;
    }

    public void initialise(Context context, LoaderCallbackInterface loaderCallbackInterface) {
        boolean success = OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, context, loaderCallbackInterface);
        Log.d(TAG, "initialising open cv: " + success);
    }

    public OpenCvResult analyse(byte[] imageData) throws NoIdentifiedAreaException {
        Bitmap image = Util.toBitmap(imageData);
        return run(image, template, Imgproc.TM_CCOEFF);
    }

    private OpenCvResult run(Bitmap inFile, Bitmap templateFile, int match_method) throws NoIdentifiedAreaException {
        System.out.println("\nRunning Template Matching");

        Log.d(TAG, "inFile width: " + inFile.getWidth() + ", height: " + inFile.getHeight());
        Log.d(TAG, "templateFile width: " + templateFile.getWidth() + ", height: " + templateFile.getHeight());


        Mat img = new Mat();
        Mat template = new Mat();

        Utils.bitmapToMat(inFile, img);
        Utils.bitmapToMat(templateFile, template);

        Log.d(TAG, "files loaded");
        Log.d(TAG, "inFile: " + img);
        Log.d(TAG, "templateFile: " + template);

        // / Create the result matrix
        int result_cols = img.cols() - template.cols() + 1;
        int result_rows = img.rows() - template.rows() + 1;
        Mat result = new Mat(result_rows, result_cols, CvType.CV_32FC1);

        // / Do the Matching
        Imgproc.matchTemplate(img, template, result, match_method);

        /*
         * This normalizes the result to be between 0 - 1. Affects threshold check
         */
        //Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());

        // / Localizing the best match with minMaxLoc
        Core.MinMaxLocResult mmr = Core.minMaxLoc(result);
        final double confidence;
        Point matchLoc;
        if (match_method == Imgproc.TM_SQDIFF || match_method == Imgproc.TM_SQDIFF_NORMED) {
            matchLoc = mmr.minLoc;
            confidence = mmr.minVal;
        } else {
            matchLoc = mmr.maxLoc;
            confidence = mmr.maxVal;
        }

        Log.d(TAG, "confidence: " + confidence);
        if (confidence < CONFIDENCE_THRESHOLD) {
            throw new NoIdentifiedAreaException();
        }

        final Bitmap visual;
        if(drawResult) {
            Imgproc.rectangle( img, matchLoc, new Point( matchLoc.x + template.cols() , matchLoc.y + template.rows() ), new Scalar(0, 255, 0), 1, 8, 0 );
            Bitmap.Config conf = Bitmap.Config.ARGB_8888;
            Bitmap bmp = Bitmap.createBitmap(inFile.getWidth(), inFile.getHeight(), conf);
            Utils.matToBitmap(img, bmp);
            visual = bmp;
        } else {
            visual = inFile;
        }

        IdentifiedArea identifiedArea = getIdentifiedArea(matchLoc, template);

        return new OpenCvResult(identifiedArea, visual);
    }

    private IdentifiedArea getIdentifiedArea(Point start, Mat template) {
        return new IdentifiedArea((int) start.x, (int) start.x + template.cols(), (int) start.y, (int) start.y + template.rows());
    }
}
