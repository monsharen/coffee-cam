package com.flexionmobile.coffee.extensions;

import android.content.Context;
import android.content.Intent;
import com.flexionmobile.coffee.extensionapi.Extension;
import com.flexionmobile.coffee.extensionapi.storage.StorageAware;
import com.flexionmobile.coffee.extensionapi.service.AsyncServiceExtension;
import com.flexionmobile.coffee.extensionapi.service.ServiceExtension;
import com.flexionmobile.coffee.extensionapi.storage.ValueType;
import com.flexionmobile.coffee.extensions.camera.CameraServiceExtension;
import com.flexionmobile.coffee.extensions.coffeecalculation.CoffeeStatisticsExtension;
import com.flexionmobile.coffee.extensions.flurry.FlurryExtension;
import com.flexionmobile.coffee.extensions.motiondetection.MotionDetectionExtension;
import com.flexionmobile.coffee.extensions.opencv.ImageRecognitionExtension;
import com.flexionmobile.coffee.extensions.motiondetection.MotionDetectionService;
import com.flexionmobile.coffee.extensions.speech.SpeechExtension;
import com.flexionmobile.coffee.extensions.webserver.WebserverExtension;
import com.flexionmobile.coffee.storage.StorageService;

import java.util.*;

public class ServiceExtensionLoader {

    private final Context context;

    private Set<Extension> extensions = new LinkedHashSet<>();
    private Set<ServiceExtension> syncServiceExtensions = new LinkedHashSet<>();
    private Set<AsyncServiceExtension> asyncServiceExtensions = new LinkedHashSet<>();

    public ServiceExtensionLoader(Context context) {
        this.context = context;
    }

    public void initialise() {
        extensions.add(new FlurryExtension());

        syncServiceExtensions.add(new CameraServiceExtension());
        syncServiceExtensions.add(new CoffeeStatisticsExtension());
        syncServiceExtensions.add(new ImageRecognitionExtension());
        syncServiceExtensions.add(new WebserverExtension());
        syncServiceExtensions.add(new MotionDetectionExtension());

        asyncServiceExtensions.add(new SpeechExtension());

        for(Extension extension : extensions) {
            injectStorageService(extension);
            extension.initialise(context);
        }

        for (ServiceExtension extension : syncServiceExtensions) {
            injectStorageService(extension);
            extension.initialise(context);
        }

        for (ServiceExtension asyncServiceExtension : asyncServiceExtensions) {
            injectStorageService(asyncServiceExtension);
            asyncServiceExtension.initialise(context);
        }
    }

    public Set<ServiceExtension> getSynchronousServiceExtensions() {
        return syncServiceExtensions;
    }

    public Set<AsyncServiceExtension> getAsynchronousServiceExtensions() {
        return asyncServiceExtensions;
    }

    public List<Intent> getSynchronousIntentsToStart() {

        List<Intent> services = getExtensionIntents(syncServiceExtensions);
        services.add(new Intent(context, MotionDetectionService.class));

        return services;
    }

    public List<Intent> getIntentsToStop() {
        List<Intent> services = getExtensionIntents(syncServiceExtensions);
        for (AsyncServiceExtension asyncServiceExtension : asyncServiceExtensions) {
            services.add(asyncServiceExtension.getServiceIntent(context));
        }
        services.add(new Intent(context, MotionDetectionService.class));
        return services;
    }

    public Map<String, Map<String, ValueType>> getAllExtensionConfiguration() {
        Map<String, Map<String, ValueType>> configuration = new HashMap<>();
        for(Extension extension : extensions) {
            if (extension instanceof StorageAware) {
                StorageAware storageAware = (StorageAware) extension;
                Map<String, ValueType> supportedStorageValues = storageAware.getSupportedStorageValues();
                configuration.put(storageAware.getId(), supportedStorageValues);
            }
        }

        for (ServiceExtension extension : syncServiceExtensions) {
            if (extension instanceof StorageAware) {
                StorageAware storageAware = (StorageAware) extension;
                Map<String, ValueType> supportedStorageValues = storageAware.getSupportedStorageValues();
                configuration.put(storageAware.getId(), supportedStorageValues);
            }
        }

        for (ServiceExtension extension : asyncServiceExtensions) {
            if (extension instanceof StorageAware) {
                StorageAware storageAware = (StorageAware) extension;
                Map<String, ValueType> supportedStorageValues = storageAware.getSupportedStorageValues();
                configuration.put(storageAware.getId(), supportedStorageValues);
            }
        }
        return configuration;
    }

    private void injectStorageService(Extension extension) {
        if (extension instanceof StorageAware) {
            String id = ((StorageAware) extension).getId();
            StorageService storageService = new StorageService(context, id);
            ((StorageAware) extension).acceptStorageService(storageService);
        }
    }

    private List<Intent> getExtensionIntents(Set<ServiceExtension> serviceExtensions) {
        List<Intent> intents = new ArrayList<>();
        for (ServiceExtension extension : serviceExtensions) {
            Intent intent = extension.getServiceIntent(context);
            intents.add(intent);
        }
        return intents;
    }
}
