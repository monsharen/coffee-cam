package com.flexionmobile.coffee.extensions.serverlog;

import com.flexionmobile.coffee.LogMessage;
import com.flexionmobile.coffee.coffeecam.CoffeeState;
import com.flexionmobile.coffee.webserver.RequestException;
import com.flexionmobile.coffee.webserver.WebserverPlugin;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fi.iki.elonen.NanoHTTPD;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class LogServerPlugin implements WebserverPlugin {

    private static final String TAG = "LogServerPlugin";
    private final CoffeeState coffeeState;

    public LogServerPlugin(CoffeeState coffeeState) {
        this.coffeeState = coffeeState;
    }

    @Override
    public String getUri() {
        return "/logserver/list";
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) throws RequestException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

        Gson gSon = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("server", getServer(simpleDateFormat, gSon));
        } catch (JSONException e) {
            throw new RequestException();
        }
        return newFixedLengthResponse(NanoHTTPD.Response.Status.OK, "application/json", jsonObject.toString());
    }

    private JSONObject getServer(SimpleDateFormat simpleDateFormat, Gson gSon) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("runningSince", simpleDateFormat.format(coffeeState.getRunningSince()));

        JSONArray jsonArray = new JSONArray();
        for (LogMessage logMessage : coffeeState.getLogMessages()) {
            String json = gSon.toJson(logMessage);
            JSONObject logJson = new JSONObject(json);
            jsonArray.put(logJson);
        }
        jsonObject.put("logMessages", jsonArray);

        return jsonObject;
    }
}
