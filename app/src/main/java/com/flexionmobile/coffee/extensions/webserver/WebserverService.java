package com.flexionmobile.coffee.extensions.webserver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Parcelable;
import android.util.Log;
import com.flexionmobile.coffee.Photo;
import com.flexionmobile.coffee.coffeecam.CoffeeState;
import com.flexionmobile.coffee.coffeecam.webserver.ImageWebserverPlugin;
import com.flexionmobile.coffee.coffeecam.webserver.MainPagePlugin;
import com.flexionmobile.coffee.events.*;
import com.flexionmobile.coffee.extensions.admin.AdminPlugin;
import com.flexionmobile.coffee.extensions.chatbox.JsonChatPlugin;
import com.flexionmobile.coffee.extensions.gallery.JsonStatisticsPlugin;
import com.flexionmobile.coffee.extensions.serverlog.LogServerPlugin;
import com.flexionmobile.coffee.extensions.soundboard.SoundboardPlugin;
import com.flexionmobile.coffee.services.CoffeeService;
import com.flexionmobile.coffee.storage.StorageService;
import com.flexionmobile.coffee.webserver.WebServer;

import java.io.IOException;
import java.util.Date;

public class WebserverService extends CoffeeService {

    private static final String TAG = "WebserverService";

    private WebServer webServer = null;
    private CoffeeState coffeeState;

    private ImageWebserverPlugin imageUrlPlugin;

    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate() {
        super.onCreate();

        StorageService storageService = new StorageService(this);
        coffeeState = new CoffeeState(storageService);
        try {
            coffeeState.initialise();
        } catch (Exception e) {
            throw new IllegalStateException("failed to init coffee state", e);
        }

        imageUrlPlugin = new ImageWebserverPlugin(coffeeState);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Parcelable event = intent.getParcelableExtra(Events.EVENT);

                if (event instanceof PhotoTakenEvent) {
                    coffeeState.increatePhotosTaken();
                    PhotoTakenEvent photoTakenEvent = (PhotoTakenEvent) event;
                    Photo photo = new Photo(new Date(), photoTakenEvent.getImageBytes());
                    coffeeState.setLatestImage(photo);
                } else if (event instanceof LogMessageEvent) {
                    LogMessageEvent logMessageEvent = (LogMessageEvent) event;
                    coffeeState.addLogMessage(logMessageEvent.getLogMessage());
                } else if (event instanceof MotionDetectionEvent) {
                    MotionDetectionEvent motionDetectionEvent = (MotionDetectionEvent) event;
                    byte[] imageData = motionDetectionEvent.getImageData();
                    Photo photo = new Photo(new Date(), imageData);
                    coffeeState.addToGallery(photo);
                } else if (event instanceof CoffeeStatisticsEvent) {
                    CoffeeStatisticsEvent coffeeStatisticsEvent = (CoffeeStatisticsEvent) event;
                    coffeeState.setCoffeeLeftPercentage(coffeeStatisticsEvent.getCoffeeLeftInPercentage());
                }
            }
        };
        localBroadcastManager.registerReceiver((broadcastReceiver), new IntentFilter(Events.RESULT));

        startNewWebServer();
    }

    @Override
    public void onDestroy() {
        coffeeState.persist();
        localBroadcastManager.unregisterReceiver((broadcastReceiver));
        stopWebServer();
        super.onDestroy();
    }

    private void startNewWebServer() {

        if (webServer != null) {
            webServer.closeAllConnections();
            webServer.stop();
        }

        Log.d(TAG, "starting web server...");
        try {
            webServer = new WebServer(8080);
            webServer.register(imageUrlPlugin);
            webServer.register(new JsonStatisticsPlugin(coffeeState));
            webServer.register(new MainPagePlugin(this));
            webServer.register(new JsonChatPlugin(this));
            webServer.register(new LogServerPlugin(coffeeState));
            webServer.register(new SoundboardPlugin(this));
            webServer.register(new AdminPlugin());
            coffeeState.setRunningSince(new Date());
            sendMessage(TAG, "Web server started at " + webServer.getAddress());
        } catch (IOException e) {
            Log.e(TAG, "failed to start web server", e);
            sendMessage(TAG, "Failed to start web server: " + e.getMessage());
        }
    }

    private void stopWebServer() {
        if (webServer != null) {
            webServer.closeAllConnections();
            webServer.stop();
            sendMessage(TAG, "Web server stopped");
        }
    }
}
