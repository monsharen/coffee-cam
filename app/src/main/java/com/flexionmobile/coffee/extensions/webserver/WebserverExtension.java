package com.flexionmobile.coffee.extensions.webserver;

import android.content.Context;
import android.content.Intent;
import com.flexionmobile.coffee.extensionapi.service.ServiceExtension;

import java.util.List;

public class WebserverExtension implements ServiceExtension {

    @Override
    public void initialise(Context context) {

    }

    @Override
    public void dispose() {

    }

    @Override
    public List<String> getPermissions() {
        return null;
    }

    @Override
    public Intent getServiceIntent(Context context) {
        return new Intent(context, WebserverService.class);
    }
}
