package com.flexionmobile.coffee.extensions.flurry;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import com.flexionmobile.coffee.events.Event;
import com.flexionmobile.coffee.events.Events;
import com.flurry.android.FlurryAgent;

public class FlurryBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Parcelable parcelable = intent.getParcelableExtra(Events.EVENT);
        if (parcelable instanceof Event) {
            Event event = (Event) parcelable;
            FlurryAgent.logEvent(event.getClass().getSimpleName());
        }
    }
}
