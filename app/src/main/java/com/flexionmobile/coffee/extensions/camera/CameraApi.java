package com.flexionmobile.coffee.extensions.camera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.hardware.camera2.*;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Surface;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

public class CameraApi {

    private static final String TAG = "CameraApi";

    private Handler handler;

    private CameraDevice cameraDevice;
    private String cameraId;
    private ImageReader imageReader;
    private CameraCallback currentCallback;
    private ImageReader currentImageReader;

    private final Context context;

    private boolean isReady = false;

    public CameraApi(Context context) {
        this.context = context;
    }

    public void initialise(final CameraStateCallback callback) {

        CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {

            @Override
            public void onOpened(CameraDevice camera) {
                Log.e(TAG, "onOpened");
                currentImageReader = createReader();
                cameraDevice = camera;
                handler = new Handler();
                isReady = true;
                callback.onOpen();
            }

            @Override
            public void onDisconnected(CameraDevice camera) {
                dispose();
                callback.onClosed();
            }

            @Override
            public void onError(CameraDevice camera, int error) {
                dispose();
                callback.onError(new IllegalStateException("failed to initialise camera. Error code: " + error));
            }
        };

        try {
            cameraId = manager.getCameraIdList()[0];

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                callback.onError(new IllegalStateException("User did not grant camera permission!"));
                return;
            }

            manager.openCamera(cameraId, stateCallback, handler);
        } catch (Exception e) {
            callback.onError(e);
        }
    }

    public void dispose() {
        isReady = false;
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
        handler = null;
    }

    public void takePicture(final CameraCallback callback) {
        isReady = false;

        if(null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null");
            callback.onError(new IllegalStateException("camera api must be initialised before taking pictures"));
            return;
        }

        currentCallback = callback;

        try {

            List<Surface> outputSurfaces = Collections.singletonList(currentImageReader.getSurface());

            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(currentImageReader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), null, handler);
                    } catch (Exception e) {
                        callback.onError(e);
                    }
                }
                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                    callback.onError(new IllegalStateException("failed to configure camera"));
                }
            }, handler);
        } catch (Exception e) {
           callback.onError(e);
        }
    }

    private ImageReader createReader() {
        int width = 640;
        int height = 480;
        ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 2);

        ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                Image image = null;
                try {
                    image = reader.acquireLatestImage();
                    ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                    byte[] bytes = new byte[buffer.capacity()];
                    buffer.get(bytes);
                    currentCallback.onPictureTaken(bytes);
                } catch (Exception e) {
                    Log.e(TAG, "failed to acquire image", e);
                } finally {
                    if (image != null) {
                        image.close();
                    }
                }
            }
        };
        reader.setOnImageAvailableListener(readerListener, handler);

        return reader;
    }
}
