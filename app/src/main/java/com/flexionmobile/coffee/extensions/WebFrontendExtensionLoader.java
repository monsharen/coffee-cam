package com.flexionmobile.coffee.extensions;

import android.content.Context;
import com.flexionmobile.coffee.extensionapi.web.WebFrontendExtension;
import com.flexionmobile.coffee.extensions.web.SimpleWebFrontendExtension;

import java.util.ArrayList;
import java.util.List;

public class WebFrontendExtensionLoader {

    private final Context context;

    public WebFrontendExtensionLoader(Context context) {
        this.context = context;
    }

    public List<WebFrontendExtension> getExtensions() {
        List<WebFrontendExtension> extensions = new ArrayList<>(1);
        add(extensions, new SimpleWebFrontendExtension("base"));
        add(extensions, new SimpleWebFrontendExtension("gallery"));
        add(extensions, new SimpleWebFrontendExtension("chatbox"));
        add(extensions, new SimpleWebFrontendExtension("soundboard"));
        add(extensions, new SimpleWebFrontendExtension("serverlog"));
        return extensions;
    }

    private void add(List<WebFrontendExtension> extensions, WebFrontendExtension extension) {
        extension.initialise(context);
        extensions.add(extension);
    }
}
