package com.flexionmobile.coffee.extensions.chatbox;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import com.flexionmobile.coffee.events.ChatEvent;
import com.flexionmobile.coffee.events.Event;
import com.flexionmobile.coffee.events.Events;
import com.flexionmobile.coffee.webserver.RequestException;
import com.flexionmobile.coffee.webserver.WebserverPlugin;
import fi.iki.elonen.NanoHTTPD;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class JsonChatPlugin implements WebserverPlugin {

    private static final String TAG = JsonChatPlugin.class.getSimpleName();

    private final Context context;

    public JsonChatPlugin(Context context) {
        this.context = context;
    }

    @Override
    public String getUri() {
        return "/rest/chat/say";
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) throws RequestException {
        String message = session.getParms().get("message");
        if (message != null) {
            sendEvent(new ChatEvent(message));
            return newFixedLengthResponse(NanoHTTPD.Response.Status.OK, NanoHTTPD.MIME_PLAINTEXT, "message queued");
        }

        throw new RequestException();
    }

    private void sendEvent(Event event) {
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        Intent intent = new Intent(Events.RESULT);
        intent.putExtra(Events.EVENT, event);
        localBroadcastManager.sendBroadcast(intent);
    }
}
