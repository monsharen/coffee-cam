package com.flexionmobile.coffee.extensions.gallery;

import com.flexionmobile.coffee.Photo;
import com.flexionmobile.coffee.coffeecam.CoffeeState;
import com.flexionmobile.coffee.util.Util;
import com.flexionmobile.coffee.webserver.RequestException;
import com.flexionmobile.coffee.webserver.WebserverPlugin;
import fi.iki.elonen.NanoHTTPD;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class JsonStatisticsPlugin implements WebserverPlugin {

    private static final String TAG = "RestApiPlugin";
    private final CoffeeState coffeeState;

    public JsonStatisticsPlugin(CoffeeState coffeeState) {
        this.coffeeState = coffeeState;
    }

    @Override
    public String getUri() {
        return "/rest/statistics/latest";
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) throws RequestException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("coffee", getCoffee());
            jsonObject.put("camera", getCamera(simpleDateFormat));
        } catch (JSONException e) {
            throw new RequestException();
        }
        return newFixedLengthResponse(NanoHTTPD.Response.Status.OK, "application/json", jsonObject.toString());
    }

    private JSONObject getCoffee() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("coffeeLeftPercentage", Util.toPercentage(coffeeState.getCoffeeLeftPercentage()));
        return jsonObject;
    }



    private JSONObject getCamera(SimpleDateFormat simpleDateFormat) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("photosTaken", coffeeState.getPhotosTaken());
        jsonObject.put("images", getGallery(simpleDateFormat));
        return jsonObject;
    }

    private JSONArray getGallery(SimpleDateFormat simpleDateFormat) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Photo photo : coffeeState.getGallery()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("url", "image?id=" + photo.getId());
            jsonObject.put("created", simpleDateFormat.format(photo.getCreated()));
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }
}
