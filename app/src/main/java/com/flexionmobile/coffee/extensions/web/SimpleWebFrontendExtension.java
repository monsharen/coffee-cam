package com.flexionmobile.coffee.extensions.web;

import android.content.Context;
import com.flexionmobile.coffee.extensionapi.web.WebFrontendExtension;
import com.flexionmobile.coffee.util.Util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SimpleWebFrontendExtension implements WebFrontendExtension {

    private final String id;
    private Context context;
    private String body;
    private String css;
    private String head;

    public SimpleWebFrontendExtension(String id) {
        this.id = id;
    }

    @Override
    public void initialise(Context context) {
        this.context = context;
        head = load("extensions/" + id + "/head.html");
        css = load("extensions/" + id + "/styles.css");
        body = load("extensions/" + id + "/body.html");
    }

    @Override
    public void dispose() {
        context = null;
        body = null;
        css = null;
        head = null;
    }

    @Override
    public List<String> getPermissions() {
        return new ArrayList<>(0);
    }

    @Override
    public String getHeadElements() {
        return head;
    }

    @Override
    public String getCss() {
        return css;
    }

    @Override
    public String getBody() {
        return body;
    }

    private String load(String filePath) {
        try {
            InputStream open = context.getApplicationContext().getAssets().open(filePath);
            return Util.read(open);
        } catch (IOException e) {
            throw new IllegalStateException("failed to load asset for plugin with id '" + id + "': " + filePath);
        }
    }
}
