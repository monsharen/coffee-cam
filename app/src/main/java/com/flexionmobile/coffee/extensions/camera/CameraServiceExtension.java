package com.flexionmobile.coffee.extensions.camera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import com.flexionmobile.coffee.extensionapi.service.ServiceExtension;

import java.util.ArrayList;
import java.util.List;

public class CameraServiceExtension implements ServiceExtension {

    @Override
    public void initialise(Context context) {
    }

    @Override
    public void dispose() {

    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<>(1);
        permissions.add(Manifest.permission.CAMERA);
        return permissions;
    }

    @Override
    public Intent getServiceIntent(Context context) {
        return new Intent(context, CameraService.class);
    }
}
