package com.flexionmobile.coffee.extensions.camera;

import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import com.flexionmobile.coffee.extensions.camera.CameraApi;
import com.flexionmobile.coffee.extensions.camera.CameraCallback;
import com.flexionmobile.coffee.extensions.camera.CameraStateCallback;
import com.flexionmobile.coffee.events.PhotoTakenEvent;
import com.flexionmobile.coffee.services.CoffeeService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CameraService extends CoffeeService {

    private static final String TAG = "CoffeeService";

    private static final long TAKE_PHOTO_EVERY_IN_SECONDS = 5;

    private CameraApi cameraApi = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initialiseCamera();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cameraApi != null) {
            cameraApi.dispose();
        }


    }

    private void initialiseCamera() {
        Log.d(TAG, "initialiseCamera");

        if (cameraApi != null) {
            cameraApi.dispose();
            cameraApi = null;
        }

        cameraApi = new CameraApi(getApplicationContext());
        cameraApi.initialise(new CameraStateCallback() {
            @Override
            public void onOpen() {
                Log.d(TAG, "camera api is open");
                startTakingPhotos();
            }

            @Override
            public void onClosed() {
                addMessage("Camera API was closed");
            }

            @Override
            public void onError(Exception e) {
                addMessage("Camera API failed: " + e.getMessage());
            }
        });
    }

    private void addMessage(String message) {
        Log.d(TAG, message);
    }

    private void startTakingPhotos() {

        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        scheduler.scheduleAtFixedRate(new CameraRunnable(), 0, TAKE_PHOTO_EVERY_IN_SECONDS, TimeUnit.SECONDS);
    }


    private class CameraRunnable implements Runnable {

        private boolean isReady = true;

        @Override
        public void run() {
            if (!isReady) {
                Log.d(TAG, "busy with task. come back later");
                return;
            }

            isReady = false;
            Log.d(TAG, "running task");
            try {
                cameraApi.takePicture(new CameraCallback() {
                    @Override
                    public void onError(Exception e) {
                        Log.e(TAG, "camera error", e);
                        isReady = true;
                    }

                    @Override
                    public void onPictureTaken(byte[] imageData) {
                        PhotoTakenEvent photoTakenEvent = new PhotoTakenEvent(imageData);
                        sendEvent(photoTakenEvent);
                        isReady = true;
                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "failed to schedule camera", e);
                isReady = true;
            }
        }
    }
}
