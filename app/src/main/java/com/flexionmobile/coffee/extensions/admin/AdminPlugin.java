package com.flexionmobile.coffee.extensions.admin;

import com.flexionmobile.coffee.extensionapi.storage.ValueType;
import com.flexionmobile.coffee.extensions.ServiceExtensionLoader;
import com.flexionmobile.coffee.webserver.RequestException;
import com.flexionmobile.coffee.webserver.WebserverPlugin;
import fi.iki.elonen.NanoHTTPD;

import java.util.Map;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class AdminPlugin implements WebserverPlugin {

    private final ServiceExtensionLoader serviceExtensionLoader;

    public AdminPlugin(ServiceExtensionLoader serviceExtensionLoader) {
        this.serviceExtensionLoader = serviceExtensionLoader;
    }

    @Override
    public String getUri() {
        return "/admin";
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) throws RequestException {
        Map<String, Map<String, ValueType>> allExtensionConfiguration = serviceExtensionLoader.getAllExtensionConfiguration();
        return newFixedLengthResponse("Hello admin world");
    }
}
