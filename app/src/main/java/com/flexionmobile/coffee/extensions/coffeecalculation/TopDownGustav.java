package com.flexionmobile.coffee.extensions.coffeecalculation;

import android.graphics.Bitmap;
import android.util.Log;

public class TopDownGustav implements Gustav {

    private static final String TAG = "TopDownGustav";

    private static final int MINIMUM_ROW_LENGTH_IN_PIXELS = 90;

    private static final int BLACK_THRESHOLD = 40;

    public GustavResult analyse(Bitmap bitmap) {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        System.out.println("Starting Top Down pot analysis...");

        int startY = height / 4;

        for (int y = startY; y < height; y++) {

            int blackPixelsInARow = 0;
            for (int x = 0; x < width; x++) {
                int rgb = bitmap.getPixel(x, y);
                Color color = new Color(rgb);
                if (isBlackEnough(color)) {
                    blackPixelsInARow++;
                } else {
                    blackPixelsInARow = 0;
                }
            }

            if (blackPixelsInARow > MINIMUM_ROW_LENGTH_IN_PIXELS) {
                int maxCoffeeLevelInPixelHeight = startY * 3;

                float amountOfCoffeeLeftInPercentage = getAmountOfCoffeeLeftInPercentage(y - startY, maxCoffeeLevelInPixelHeight);
                Log.d(TAG, "We found a row of black pixels (" + blackPixelsInARow + " out of " + width + ") at y " + (y - startY) + " of " + maxCoffeeLevelInPixelHeight);
                return new GustavResult(amountOfCoffeeLeftInPercentage);
            }
        }

        return new GustavResult(0.0f);
    }

    private float getAmountOfCoffeeLeftInPercentage(int firstIdentifiedCoffeeY, int height) {
        return ((float) (height - firstIdentifiedCoffeeY)) / ((float) height);
    }

    private boolean isBlackEnough(Color color) {
        return color.getRed() < BLACK_THRESHOLD && color.getGreen() < BLACK_THRESHOLD && color.getBlue() < BLACK_THRESHOLD;
    }
}
