package com.flexionmobile.coffee.extensions.camera;

public interface CameraCallback {

    void onError(Exception e);
    void onPictureTaken(byte[] imageData);
}
