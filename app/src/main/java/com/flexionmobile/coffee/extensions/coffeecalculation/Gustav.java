package com.flexionmobile.coffee.extensions.coffeecalculation;

import android.graphics.Bitmap;

/*
 * Gustav - The experimental coffee recognition framework
 * Why the name? https://en.wikipedia.org/wiki/Gustav_III_of_Sweden%27s_coffee_experiment
 */
public interface Gustav {

    GustavResult analyse(Bitmap bitmap);


}
