package com.flexionmobile.coffee.extensions.soundboard;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;
import com.flexionmobile.coffee.webserver.RequestException;
import com.flexionmobile.coffee.webserver.WebserverPlugin;
import fi.iki.elonen.NanoHTTPD;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class SoundboardPlugin implements WebserverPlugin {

    private static final String TAG = "SoundboardPlugin";
    private Context context;

    public SoundboardPlugin(Context context) {
        this.context = context;
    }

    @Override
    public String getUri() {
        return "/soundboard/play";
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) throws RequestException {
        String id = session.getParms().get("id");
        if (id == null) {
            throw new RequestException();
        }

        playSound(id);
        return newFixedLengthResponse(NanoHTTPD.Response.Status.OK, "application/json", "{\"result\":\"ok\"}");
    }

    private void playSound(String id) throws RequestException {

        try {
            AssetFileDescriptor afd = context.getAssets().openFd("extensions/soundboard/" + id);
            final MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer.release();
                }
            });
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                }
            });
            mediaPlayer.prepareAsync();
        } catch (Exception e) {
            Log.e(TAG, "failed to load sound on path", e);
            throw new RequestException();
        }
    }
}
