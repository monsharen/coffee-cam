package com.flexionmobile.coffee.extensions.coffeecalculation;

import com.flexionmobile.coffee.util.Util;

public class GustavResult {

    private final float amountOfCoffeeLeftInPercentage;

    GustavResult(float amountOfCoffeeLeftInPercentage) {
        this.amountOfCoffeeLeftInPercentage = amountOfCoffeeLeftInPercentage;
    }

    public float getAmountOfCoffeeLeftInPercentage() {
        return amountOfCoffeeLeftInPercentage;
    }

    public String getAmountOfCoffeeLeftAsString() {
        return Util.toPercentage(amountOfCoffeeLeftInPercentage);
    }
}
