package com.flexionmobile.coffee.extensions.motiondetection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Parcelable;
import android.util.Log;
import com.flexionmobile.coffee.events.ImageRecognitionEvent;
import com.flexionmobile.coffee.events.MotionDetectionEndedEvent;
import com.flexionmobile.coffee.events.MotionDetectionEvent;
import com.flexionmobile.coffee.events.MotionDetectionStartedEvent;
import com.flexionmobile.coffee.events.Events;
import com.flexionmobile.coffee.IdentifiedArea;
import com.flexionmobile.coffee.services.CoffeeService;

public class MotionDetectionService extends CoffeeService {

    private static final String TAG = "MotionDetectionService";
    private BroadcastReceiver broadcastReceiver;

    private StateMachine stateMachine = new StateMachine();

    @Override
    public void onCreate() {
        super.onCreate();

        stateMachine.init();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Parcelable event = intent.getParcelableExtra(Events.EVENT);
                if (event instanceof ImageRecognitionEvent) {
                    ImageRecognitionEvent imageRecognitionEvent = (ImageRecognitionEvent) event;

                    IdentifiedArea identifiedArea = imageRecognitionEvent.getIdentifiedArea();
                    byte[] imageBytes = imageRecognitionEvent.getImageBytes();
                    stateMachine.process(identifiedArea, imageBytes);
                }
            }
        };
        localBroadcastManager.registerReceiver((broadcastReceiver), new IntentFilter(Events.RESULT));
    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private class StateMachine {
        private State currentState;

        void init() {
            currentState = new IdleState(new IdentifiedArea(0, 0, 0, 0));
        }

        void process(IdentifiedArea identifiedArea, byte[] image) {
            currentState.process(identifiedArea, image);
        }

        void changeState(State state) {
            currentState.end();
            log("Switching state to " + state.getClass().getSimpleName());
            currentState = state;
            currentState.start();
        }

        void log(String message) {
            Log.d(TAG, message);
        }
    }

    private interface State {
        void start();
        void process(IdentifiedArea identifiedArea, byte[] imageBytes);
        void end();
    }

    private class IdleState extends BaseState {

        IdleState(IdentifiedArea lastIdentifiedArea) {
            super(lastIdentifiedArea);
        }

        @Override
        public void start() {

        }

        @Override
        public void process(IdentifiedArea identifiedArea, byte[] image) {

            if (isWithinTolerance(identifiedArea)) {
                return;
            }

            stateMachine.changeState(new MotionState(identifiedArea, image));
        }

        @Override
        public void end() {

        }
    }

    private class MotionState extends BaseState {

        private final byte[] startImage;

        MotionState(IdentifiedArea lastIdentifiedArea, byte[] image) {
            super(lastIdentifiedArea);
            this.startImage = image;
        }

        @Override
        public void start() {
            sendEvent(new MotionDetectionStartedEvent());
            sendEvent(new MotionDetectionEvent(startImage));
        }

        @Override
        public void process(IdentifiedArea identifiedArea, byte[] image) {
            sendEvent(new MotionDetectionEvent(image));
            Log.d(TAG, "MotionState: " + identifiedArea + ", " + lastIdentifiedArea);
            if (isWithinTolerance(identifiedArea)) {
                stateMachine.changeState(new IdleState(identifiedArea));
                sendEvent(new MotionDetectionEndedEvent(image, identifiedArea));
            }

            lastIdentifiedArea = identifiedArea;
        }

        @Override
        public void end() {

        }
    }

    private abstract class BaseState implements State {

        private static final int TOLERANCE = 20;

        IdentifiedArea lastIdentifiedArea;

        BaseState(IdentifiedArea lastIdentifiedArea) {
            this.lastIdentifiedArea = lastIdentifiedArea;
        }

        boolean isWithinTolerance(IdentifiedArea identifiedArea) {

            if (identifiedArea.equals(lastIdentifiedArea)) {
                return true;
            }

            int startX = identifiedArea.getStartX();
            int startY = identifiedArea.getStartY();

            int x = lastIdentifiedArea.getStartX();
            int y = lastIdentifiedArea.getStartY();

            return isWithinRange(startX, x - TOLERANCE, x + TOLERANCE) && isWithinRange(startY, y - TOLERANCE, y + TOLERANCE);
        }

        private boolean isWithinRange(int value, int start, int end) {
            return (value >= start && value <= end);
        }


    }

}
