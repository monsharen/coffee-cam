package com.flexionmobile.coffee.extensions.camera;

public interface CameraStateCallback {

    void onOpen();
    void onClosed();
    void onError(Exception e);
}
