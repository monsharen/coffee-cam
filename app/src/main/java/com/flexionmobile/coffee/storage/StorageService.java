package com.flexionmobile.coffee.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class StorageService {

    private final Context context;
    private final String extensionId;
    private final SharedPreferences sharedPreferences;

    public StorageService(Context context) {
        this(context, "global");
    }

    public StorageService(Context context, String extensionId) {
        this.context = context;
        this.extensionId = extensionId;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getString(String key, String defaultValue) {
        return sharedPreferences.getString(getExtensionSpecificKey(key), defaultValue);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getExtensionSpecificKey(key), value);
        editor.apply();
    }

    public int getInteger(String key, int defaultValue) {
        return sharedPreferences.getInt(getExtensionSpecificKey(key), defaultValue);
    }

    public void setInteger(String key, int value) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(getExtensionSpecificKey(key), value);
        edit.apply();
    }

    public float getFloat(String key, float defaultValue) {
        return sharedPreferences.getFloat(getExtensionSpecificKey(key), defaultValue);
    }

    public void setFloat(String key, float value) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putFloat(getExtensionSpecificKey(key), value);
        edit.apply();
    }

    public Date getDate(String key) throws Exception {
        String dateString = sharedPreferences.getString(getExtensionSpecificKey(key), "1970-01-01 00:00:00");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
        return simpleDateFormat.parse(dateString);
    }

    public void setDate(String key, Date value) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
        String formattedDate = simpleDateFormat.format(value);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(getExtensionSpecificKey(key), formattedDate);
        edit.apply();
    }

    public byte[] getBytes(String key) {
        String data = sharedPreferences.getString(getExtensionSpecificKey(key), "");
        byte[] decode = Base64.decode(data.getBytes(), Base64.DEFAULT);
        return decode;
    }

    public void setBytes(String key, byte[] bytes) {
        String encode = Base64.encodeToString(bytes, Base64.DEFAULT);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(getExtensionSpecificKey(key), encode);
        edit.apply();
    }

    private String getExtensionSpecificKey(String key) {
        return extensionId + "_" + key;
    }
}
