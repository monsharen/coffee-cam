package com.flexionmobile.coffee.storage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public interface Persistable {

    void read(DataInputStream dataInputStream) throws IOException;

    void write(DataOutputStream dataOutputStream) throws IOException;
}
