package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.flexionmobile.coffee.LogMessage;

public class LogMessageEvent implements Event {

    public static final Parcelable.Creator<LogMessageEvent> CREATOR = new Parcelable.Creator<LogMessageEvent>() {
        public LogMessageEvent createFromParcel(Parcel in) {
            return new LogMessageEvent(in);
        }

        public LogMessageEvent[] newArray(int size) {
            return new LogMessageEvent[size];
        }
    };

    private final LogMessage logMessage;

    private LogMessageEvent(Parcel in) {
        logMessage = (LogMessage) in.readSerializable();
    }

    public LogMessageEvent(LogMessage logMessage) {
        this.logMessage = logMessage;
    }

    public LogMessage getLogMessage() {
        return logMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(logMessage);
    }
}
