package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.flexionmobile.coffee.IdentifiedArea;

public class MotionDetectionEndedEvent implements Event {

    public static final Parcelable.Creator<MotionDetectionEndedEvent> CREATOR = new Parcelable.Creator<MotionDetectionEndedEvent>() {
        public MotionDetectionEndedEvent createFromParcel(Parcel in) {
            return new MotionDetectionEndedEvent(in);
        }

        public MotionDetectionEndedEvent[] newArray(int size) {
            return new MotionDetectionEndedEvent[size];
        }
    };

    private final byte[] imageBytes;
    private final IdentifiedArea identifiedArea;

    private MotionDetectionEndedEvent(Parcel in) {
        int length = in.readInt();
        imageBytes = new byte[length];
        in.readByteArray(imageBytes);
        identifiedArea = (IdentifiedArea) in.readSerializable();
    }

    public MotionDetectionEndedEvent(byte[] imageBytes, IdentifiedArea identifiedArea) {
        this.imageBytes = imageBytes;
        this.identifiedArea = identifiedArea;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageBytes.length);
        dest.writeByteArray(imageBytes);
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public IdentifiedArea getIdentifiedArea() {
        return identifiedArea;
    }
}
