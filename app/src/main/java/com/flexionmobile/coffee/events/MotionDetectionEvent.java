package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;

public class MotionDetectionEvent implements Event {

    public static final Parcelable.Creator<MotionDetectionEvent> CREATOR = new Parcelable.Creator<MotionDetectionEvent>() {
        public MotionDetectionEvent createFromParcel(Parcel in) {
            return new MotionDetectionEvent(in);
        }

        public MotionDetectionEvent[] newArray(int size) {
            return new MotionDetectionEvent[size];
        }
    };

    private final byte[] imageData;

    public MotionDetectionEvent(byte[] imageData) {
        this.imageData = imageData;
    }

    private MotionDetectionEvent(Parcel in) {
        int i = in.readInt();
        imageData = new byte[i];
        in.readByteArray(imageData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageData.length);
        dest.writeByteArray(imageData);
    }

    public byte[] getImageData() {
        return imageData;
    }
}
