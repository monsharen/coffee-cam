package com.flexionmobile.coffee.events;

public class Events {

    public static final String RESULT = "com.flexionmobile.coffee.coffeecam.logging.REQUEST_PROCESSED";
    public static final String EVENT = "com.flexionmobile.coffee.coffeecam.logging.MSG";
}
