package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;

public class ChatEvent implements Event {

    public static final Parcelable.Creator<ChatEvent> CREATOR = new Parcelable.Creator<ChatEvent>() {
        public ChatEvent createFromParcel(Parcel in) {
            return new ChatEvent(in);
        }

        public ChatEvent[] newArray(int size) {
            return new ChatEvent[size];
        }
    };

    private final String message;

    private ChatEvent(Parcel in) {
        message = in.readString();
    }

    public ChatEvent(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }

    public String getMessage() {
        return message;
    }
}
