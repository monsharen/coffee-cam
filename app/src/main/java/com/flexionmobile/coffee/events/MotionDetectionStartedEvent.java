package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;

public class MotionDetectionStartedEvent implements Event {

    public static final Parcelable.Creator<MotionDetectionStartedEvent> CREATOR = new Parcelable.Creator<MotionDetectionStartedEvent>() {
        public MotionDetectionStartedEvent createFromParcel(Parcel in) {
            return new MotionDetectionStartedEvent(in);
        }

        public MotionDetectionStartedEvent[] newArray(int size) {
            return new MotionDetectionStartedEvent[size];
        }
    };

    private MotionDetectionStartedEvent(Parcel in) {

    }

    public MotionDetectionStartedEvent() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

}
