package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;

public class PhotoTakenEvent implements Event {

    public static final Parcelable.Creator<PhotoTakenEvent> CREATOR = new Parcelable.Creator<PhotoTakenEvent>() {
        public PhotoTakenEvent createFromParcel(Parcel in) {
            return new PhotoTakenEvent(in);
        }

        public PhotoTakenEvent[] newArray(int size) {
            return new PhotoTakenEvent[size];
        }
    };

    private final byte[] imageBytes;

    private PhotoTakenEvent(Parcel in) {
        int length = in.readInt();
        imageBytes = new byte[length];
        in.readByteArray(imageBytes);
    }

    public PhotoTakenEvent(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageBytes.length);
        dest.writeByteArray(imageBytes);
    }
}
