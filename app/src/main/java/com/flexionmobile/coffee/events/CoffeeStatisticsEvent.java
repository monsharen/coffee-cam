package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;

public class CoffeeStatisticsEvent implements Event {

    public static final Parcelable.Creator<CoffeeStatisticsEvent> CREATOR = new Parcelable.Creator<CoffeeStatisticsEvent>() {
        public CoffeeStatisticsEvent createFromParcel(Parcel in) {
            return new CoffeeStatisticsEvent(in);
        }

        public CoffeeStatisticsEvent[] newArray(int size) {
            return new CoffeeStatisticsEvent[size];
        }
    };

    private final float coffeeLeftInPercentage;

    private CoffeeStatisticsEvent(Parcel in) {
        this.coffeeLeftInPercentage = in.readFloat();
    }

    public CoffeeStatisticsEvent(float coffeeLeftInPercentage) {
        this.coffeeLeftInPercentage = coffeeLeftInPercentage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(coffeeLeftInPercentage);
    }

    public float getCoffeeLeftInPercentage() {
        return coffeeLeftInPercentage;
    }
}
