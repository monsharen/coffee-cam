package com.flexionmobile.coffee.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.flexionmobile.coffee.IdentifiedArea;

public class ImageRecognitionEvent implements Event {

    public static final Parcelable.Creator<ImageRecognitionEvent> CREATOR = new Parcelable.Creator<ImageRecognitionEvent>() {
        public ImageRecognitionEvent createFromParcel(Parcel in) {
            return new ImageRecognitionEvent(in);
        }

        public ImageRecognitionEvent[] newArray(int size) {
            return new ImageRecognitionEvent[size];
        }
    };

    private final byte[] imageBytes;
    private final IdentifiedArea identifiedArea;

    private ImageRecognitionEvent(Parcel in) {
        int length = in.readInt();
        imageBytes = new byte[length];
        in.readByteArray(imageBytes);
        identifiedArea = (IdentifiedArea) in.readSerializable();
    }

    public ImageRecognitionEvent(byte[] imageBytes, IdentifiedArea identifiedArea) {
        this.imageBytes = imageBytes;
        this.identifiedArea = identifiedArea;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageBytes.length);
        dest.writeByteArray(imageBytes);
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public IdentifiedArea getIdentifiedArea() {
        return identifiedArea;
    }

}
