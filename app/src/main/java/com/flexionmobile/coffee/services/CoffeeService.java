package com.flexionmobile.coffee.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.flexionmobile.coffee.LogMessage;
import com.flexionmobile.coffee.events.LogMessageEvent;
import com.flexionmobile.coffee.events.Event;
import com.flexionmobile.coffee.events.Events;

import java.util.Date;

public class CoffeeService extends Service {

    protected LocalBroadcastManager localBroadcastManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    public void sendEvent(Event event) {
        Intent intent = new Intent(Events.RESULT);
        intent.putExtra(Events.EVENT, event);
        localBroadcastManager.sendBroadcast(intent);
    }

    public void sendMessage(String tag, String message) {
        Log.d(tag, "sending " + message);
        LogMessage logMessage = new LogMessage(new Date(), message);
        LogMessageEvent logMessageEvent = new LogMessageEvent(logMessage);
        sendEvent(logMessageEvent);
    }
}
