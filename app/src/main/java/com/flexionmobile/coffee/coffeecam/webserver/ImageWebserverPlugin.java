package com.flexionmobile.coffee.coffeecam.webserver;

import com.flexionmobile.coffee.Photo;
import com.flexionmobile.coffee.coffeecam.CoffeeState;
import com.flexionmobile.coffee.webserver.WebserverPlugin;
import fi.iki.elonen.NanoHTTPD;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class ImageWebserverPlugin implements WebserverPlugin {

    private final CoffeeState coffeeState;

    public ImageWebserverPlugin(CoffeeState coffeeState) {
        this.coffeeState = coffeeState;
    }

    @Override
    public String getUri() {
        return "/image";
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) {

        String id = session.getParms().get("id");

        if ("latest".equals(id) && coffeeState.getLatestImage() != null) {
            return returnPhotoResponse(coffeeState.getLatestImage());
        }

        Photo photo = getPhoto(id);

        if (photo == null) {
            return newFixedLengthResponse(NanoHTTPD.Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "not found");
        }

        return returnPhotoResponse(photo);
    }

    private NanoHTTPD.Response returnPhotoResponse(Photo photo) {
        InputStream inputStream = new ByteArrayInputStream(photo.getData());

        return newFixedLengthResponse(NanoHTTPD.Response.Status.OK, "image/png", inputStream, photo.getData().length);
    }

    private Photo getPhoto(String id) {
        for (Photo photo : coffeeState.getGallery()) {
            if (photo.getId().equals(id)) {
                return photo;
            }
        }
        return null;
    }
}
