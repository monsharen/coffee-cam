package com.flexionmobile.coffee.coffeecam.webserver;

import android.content.Context;
import com.flexionmobile.coffee.extensionapi.web.WebFrontendExtension;
import com.flexionmobile.coffee.extensions.WebFrontendExtensionLoader;
import com.flexionmobile.coffee.webserver.Page;
import com.flexionmobile.coffee.webserver.WebserverPlugin;
import fi.iki.elonen.NanoHTTPD;

import java.util.List;

import static fi.iki.elonen.NanoHTTPD.newFixedLengthResponse;

public class MainPagePlugin implements WebserverPlugin {

    private Page page;
    private final List<WebFrontendExtension> webFrontendExtensions;

    public MainPagePlugin(Context context) {
        WebFrontendExtensionLoader loader = new WebFrontendExtensionLoader(context);
        webFrontendExtensions = loader.getExtensions();
        page = generatePageFromExtensions();
    }

    @Override
    public String getUri() {
        return "/";
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) {
        return newFixedLengthResponse(page.getContent());
    }

    private Page generatePageFromExtensions() {

        StringBuilder html = new StringBuilder();
        html.append("<html>");
        html.append("<head><title>Coffeecam</title>");
        for (WebFrontendExtension webFrontendExtension : webFrontendExtensions) {
            html.append(webFrontendExtension.getHeadElements());
        }
        html.append("<style>");
        for (WebFrontendExtension webFrontendExtension : webFrontendExtensions) {
            html.append(webFrontendExtension.getCss());
        }
        html.append("</style>");
        html.append("</head>");
        html.append("<body style='background-color:black'>");
        for (WebFrontendExtension webFrontendExtension : webFrontendExtensions) {
            html.append(getExtensionWrapperDiv());
            html.append(webFrontendExtension.getBody());
            html.append("</div>");
        }
        html.append("</body></html>");

        return new Page(html.toString());
    }

    private String getExtensionWrapperDiv() {
        return "<div class='extension'>";
    }

}
