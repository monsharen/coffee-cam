package com.flexionmobile.coffee.coffeecam;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.flexionmobile.coffee.LogMessage;
import com.flexionmobile.coffee.events.Events;
import com.flexionmobile.coffee.events.LogMessageEvent;
import com.flexionmobile.coffee.extensionapi.service.AsyncServiceExtension;
import com.flexionmobile.coffee.extensionapi.service.ServiceExtension;
import com.flexionmobile.coffee.extensions.ServiceExtensionLoader;

import java.text.SimpleDateFormat;
import java.util.*;

public class CoffeeCamActivity extends AppCompatActivity {

    private static final String TAG = "CoffeeCam";

    private static final int REQUEST_ALL_PERMISSION = 200;

    private ServiceExtensionLoader serviceExtensionLoader;

    private BroadcastReceiver broadcastReceiver;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        serviceExtensionLoader = new ServiceExtensionLoader(this);
        serviceExtensionLoader.initialise();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Parcelable event = intent.getParcelableExtra(Events.EVENT);

                if (event instanceof LogMessageEvent) {
                    LogMessageEvent logMessageEvent = (LogMessageEvent) event;
                    LogMessage logMessage = logMessageEvent.getLogMessage();
                    addLogMessage(logMessage);
                }
            }
        };

        setContentView(R.layout.activity_android_camera_api);
        textView = (TextView) findViewById(R.id.textView);
        requestPermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ALL_PERMISSION) {
            initialiseServices();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((broadcastReceiver), new IntentFilter(Events.RESULT));
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        stopServices();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Set<AsyncServiceExtension> asynchronousServiceExtensions = serviceExtensionLoader.getAsynchronousServiceExtensions();
        for (AsyncServiceExtension extension : asynchronousServiceExtensions) {
            extension.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void requestPermissions() {
        Set<ServiceExtension> serviceExtensions = serviceExtensionLoader.getSynchronousServiceExtensions();
        List<String> allPermissions = new ArrayList<>();
        boolean askPermissions = false;

        for (ServiceExtension serviceExtension : serviceExtensions) {
            List<String> permissions = serviceExtension.getPermissions();
            if (permissions != null) {
                allPermissions.addAll(permissions);
            }
        }

        for (String permission : allPermissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                askPermissions = true;
            }
        }

        if (askPermissions && allPermissions.size() != 0) {
            String[] permissions = new String[allPermissions.size()];
            for (int i = 0; i < allPermissions.size(); i++) {
                permissions[i] = allPermissions.get(i);
            }
            ActivityCompat.requestPermissions(this, permissions, REQUEST_ALL_PERMISSION);
        } else {
            initialiseServices();
        }
    }

    private void initialiseServices() {

        Set<ServiceExtension> serviceExtensions = serviceExtensionLoader.getSynchronousServiceExtensions();

        for (ServiceExtension serviceExtension : serviceExtensions) {
            List<String> permissions = serviceExtension.getPermissions();
            if (permissions != null) {
                for (String permission : permissions) {
                    if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                        Intent intent = serviceExtension.getServiceIntent(this);
                        startService(intent);
                    }
                }
            }
        }

        List<Intent> intents = serviceExtensionLoader.getSynchronousIntentsToStart();

        for (Intent intent : intents) {
            startService(intent);
        }

        Set<AsyncServiceExtension> asynchronousServiceExtensions = serviceExtensionLoader.getAsynchronousServiceExtensions();

        for (AsyncServiceExtension extension : asynchronousServiceExtensions) {
            Intent intent = extension.getActivityIntent();
            int requestCode = extension.getRequestCode();
            startActivityForResult(intent, requestCode);
        }
    }

    private void stopServices() {
        ServiceExtensionLoader serviceExtensionLoader = new ServiceExtensionLoader(this);
        List<Intent> intents = serviceExtensionLoader.getIntentsToStop();

        for (Intent intent : intents) {
            stopService(intent);
        }
    }

    private void addLogMessage(LogMessage logMessage) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
        String timeStamp = simpleDateFormat.format(logMessage.getCreated());

        textView.append(timeStamp + ": " + logMessage.getMessage() + "\n");
    }

}
