package com.flexionmobile.coffee.coffeecam;

import com.flexionmobile.coffee.LogMessage;
import com.flexionmobile.coffee.Photo;
import com.flexionmobile.coffee.services.CoffeeService;
import com.flexionmobile.coffee.storage.Persistable;
import com.flexionmobile.coffee.storage.StorageService;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CoffeeState {

    private static final int GALLERY_MAX_SIZE = 6;

    private Date runningSince = new Date();
    private int photosTaken = 0;
    private float coffeeLeftPercentage = 0f;
    private List<LogMessage> logMessages = new ArrayList<>();
    private LinkedList<Photo> gallery = new LinkedList<>();
    private Photo latestImage = null;

    private final StorageService storageService;

    public CoffeeState(StorageService storageService) {
        this.storageService = storageService;
    }

    public Date getRunningSince() {
        return runningSince;
    }

    public void setRunningSince(Date runningSince) {
        this.runningSince = runningSince;
    }

    public int getPhotosTaken() {
        return photosTaken;
    }

    public void setPhotosTaken(int photosTaken) {
        this.photosTaken = photosTaken;
    }

    public void increatePhotosTaken() {
        this.photosTaken++;
    }

    public List<LogMessage> getLogMessages() {
        return logMessages;
    }

    public void setLogMessages(List<LogMessage> logMessages) {
        this.logMessages = logMessages;
    }

    public void addLogMessage(LogMessage logMessage) {
        this.logMessages.add(logMessage);
    }

    public float getCoffeeLeftPercentage() {
        return coffeeLeftPercentage;
    }

    public void setCoffeeLeftPercentage(float coffeeLeftPercentage) {
        this.coffeeLeftPercentage = coffeeLeftPercentage;
    }

    public LinkedList<Photo> getGallery() {
        return gallery;
    }

    public void setGallery(LinkedList<Photo> gallery) {
        this.gallery = gallery;
    }

    public void addToGallery(Photo image) {
        gallery.addFirst(image);
        if (gallery.size() > GALLERY_MAX_SIZE) {
            gallery.removeLast();
        }
    }

    public Photo getLatestImage() {
        return latestImage;
    }

    public void setLatestImage(Photo latestImage) {
        this.latestImage = latestImage;
    }

    public void initialise() throws Exception {
        photosTaken = storageService.getInteger("photosTaken", 0);
        coffeeLeftPercentage = storageService.getFloat("coffeeLeftPercentage", 0L);
        int i = storageService.getInteger("numberOfLogMessages", 0);
        logMessages = new ArrayList<>(i);
        for (int x = 0; x < i; x++) {
            String message = storageService.getString("logMessage" + x, "");
            Date date = storageService.getDate("logDate" + x);
            LogMessage logMessage = new LogMessage(date, message);
            logMessages.add(logMessage);
        }
        i = storageService.getInteger("numberOfPhotos", 0);
        gallery = new LinkedList<>();
        for (int x = 0; x < i; x++) {
            Date date = storageService.getDate("photoDate" + x);
            byte[] photoData = storageService.getBytes("photoData" + x);
            Photo photo = new Photo(date, photoData);
            gallery.add(photo);
        }
        Date latestPhoto = storageService.getDate("latestPhotoDate");
        byte[] latestPhotoDatas = storageService.getBytes("latestPhotoData");
        latestImage = new Photo(latestPhoto, latestPhotoDatas);
    }

    public void persist() {
        storageService.setInteger("photosTaken", photosTaken);
        storageService.setFloat("coffeeLeftPercentage", coffeeLeftPercentage);
        storageService.setInteger("numberOfLogMessages", logMessages.size());
        for (int i=0; i < logMessages.size(); i++) {
            LogMessage logMessage = logMessages.get(i);
            storageService.setString("logMessage" + i, logMessage.getMessage());
            storageService.setDate("logDate" + i, logMessage.getCreated());
        }
        storageService.setInteger("numberOfPhotos", gallery.size());
        for (int i=0; i < gallery.size(); i++) {
            Photo photo = gallery.get(i);
            storageService.setDate("photoDate" + i, photo.getCreated());
            storageService.setBytes("photoData" + i, photo.getData());
        }
        storageService.setDate("latestPhotoDate", latestImage.getCreated());
        storageService.setBytes("latestPhotoData", latestImage.getData());
    }
}
