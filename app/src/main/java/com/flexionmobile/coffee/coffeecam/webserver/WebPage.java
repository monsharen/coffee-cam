package com.flexionmobile.coffee.coffeecam.webserver;

import android.util.Base64;
import com.flexionmobile.coffee.LogMessage;
import com.flexionmobile.coffee.webserver.Page;
import com.flexionmobile.coffee.util.Util;

import java.text.SimpleDateFormat;
import java.util.*;

public class WebPage extends Page {

    private WebPage(String content) {
        super(content);
    }

    public static class Builder {
        private final String html;
        private Date serverStarted = new Date();
        private int picturesTaken = 0;
        private float coffeeLeft  = 0f;
        private byte[] imageBytes = new byte[0];
        private List<LogMessage> logMessages = new ArrayList<>(0);
        private List<byte[]> images = new ArrayList<>(0);

        public Builder(String html) {
            this.html = html;
        }

        public Builder setServerStarted(Date serverStarted) {
            this.serverStarted = serverStarted;
            return this;
        }

        public Builder setPicturesTaken(int picturesTaken) {
            this.picturesTaken = picturesTaken;
            return this;
        }

        public Builder setCoffeeLeft(float coffeeLeft) {
            this.coffeeLeft = coffeeLeft;
            return this;
        }

        public Builder setImageBytes(byte[] imageBytes) {
            this.imageBytes = imageBytes;
            return this;
        }

        public Builder setLogMessages(List<LogMessage> logMessages) {
            this.logMessages = logMessages;
            return this;
        }

        public Builder setImages(List<byte[]> images) {
            this.images = images;
            return this;
        }

        public WebPage build() {

            String content = generateContent();
            return new WebPage(content);
        }

        private String generateContent() {
            String coffeeLeftPercentage = Util.toPercentage(coffeeLeft);
            Date lastUpdate = new Date();
            String encodedImageData = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);
            /* return "<html>" +
                    "<head>" +
                    //"<meta http-equiv='refresh' content='10'>" +
                    "</head>" +
                    "<body style='color: white; background-color: black;'>" +
                    "<div style='margin: auto; width: 640; height: 480;'>" +
                    "<h1>Flexion CoffeeCam</h1>\n" +
                    "<img style='width: 640; height: 480;' src='data:image/png;base64," + encodedImageData + "' /><br /><br />" +
                    "Coffee left: " + coffeeLeftPercentage + "<br />" +
                    "Current picture captured at: " + simpleDateFormat.format(lastUpdate) + "<br />" +
                    "Running since: " + simpleDateFormat.format(serverStarted) + "<br />" +
                    "Pictures taken since start: " + picturesTaken + "<br />" +
                    "<div style='margin-top: 2em;'>" + generateImageGallery() + "</div>" +
                    "<div style='margin-top: 2em;'>" + generateLogMessageHtml(simpleDateFormat) + "</div>" +
                    "</div>" +
                    "</body></html>\n";
                    */
            return html;
        }

        private String generateImageGallery() {
            StringBuilder html = new StringBuilder();
            html.append("<li style='list-style:none;'>");
            for (int i = images.size() - 1; i >= asd(images.size(), 4); i--) {
                byte[] image = images.get(i);
                String encodedImageData = Base64.encodeToString(image, Base64.DEFAULT);
                html.append("<ul style='padding-left: 0; padding-right: 5px; display: table-cell;'>")
                        .append("<a href='data:image/png;base64,").append(encodedImageData).append("' target=\"_blank\">")
                        .append("<img style='display:block; width:150px; height:150px' src='data:image/png;base64,").append(encodedImageData).append("' />")
                        .append("</a>")
                        .append("</ul>");
            }
            html.append("</li>");
            return html.toString();
        }

        private String generateLogMessageHtml(SimpleDateFormat simpleDateFormat) {
            StringBuilder html = new StringBuilder();
            html.append("<b>Server log</b>");
            html.append("<li style='list-style:none'>");

            for (int i = logMessages.size() - 1; i >= asd(logMessages.size(), 10); i--) {
                LogMessage message = logMessages.get(i);
                html.append("<ul style='padding-left: 0;'>").append(simpleDateFormat.format(message.getCreated())).append(": ").append(message.getMessage()).append("</ul>");
            }
            html.append("</li>");
            return html.toString();
        }

        private int asd(int size, int max) {
            if (size > max) {
                return size - max;
            }
            return 0;
        }
    }
}
