package com.flexionmobile.coffee.webserver;

import fi.iki.elonen.NanoHTTPD;

public interface WebserverPlugin {

    String getUri();

    NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) throws RequestException;
}
