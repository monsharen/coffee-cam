package com.flexionmobile.coffee.webserver;

public class Page {

    private final String content;

    public Page(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
