package com.flexionmobile.coffee.webserver;

import android.util.Log;
import fi.iki.elonen.NanoHTTPD;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.*;

public class WebServer extends NanoHTTPD {

    private static final String TAG = "CoffeeCam-WebServer";

    private Router router = new Router();

    public WebServer(int port) throws IOException {
        super(port);
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        String ipAddress = getIPAddress(true);
        Log.d(TAG, "WebServer Running! Point your browsers to http://" + ipAddress + ":" + port);
    }

    @Override
    public Response serve(IHTTPSession session) {

        try {
            return router.serve(session);
        } catch (UnknownUrlException e) {
            Log.e(TAG, "failed to handle request for uri: " + session.getUri(), e);
        }

        return newFixedLengthResponse(NanoHTTPD.Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "not found");
    }

    public String getAddress() {
        return "http://" + getIPAddress(true) + ":" + getListeningPort();
    }

    public void register(WebserverPlugin webserverPlugin) {
        router.register(webserverPlugin);
    }

    private static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    private class Router {
        private Map<String, WebserverPlugin> urlPlugins = new HashMap<>();

        void register(WebserverPlugin webserverPlugin) {
            String uri = webserverPlugin.getUri();
            urlPlugins.put(uri, webserverPlugin);
        }

        Response serve(IHTTPSession session) throws UnknownUrlException {
            WebserverPlugin webserverPlugin = urlPlugins.get(session.getUri());
            if (webserverPlugin == null) {
                throw new UnknownUrlException();
            }
            try {
                return webserverPlugin.serve(session);
            } catch (RequestException e) {
                Log.e(TAG, "failed to handle request", e);
            }

            throw new UnknownUrlException();
        }
    }

}
