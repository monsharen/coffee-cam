package com.flexionmobile.coffee.extensionapi.storage;

import com.flexionmobile.coffee.storage.StorageService;

import java.util.Map;

public interface StorageAware {

    String getId();

    void acceptStorageService(StorageService storageService);

    Map<String, ValueType> getSupportedStorageValues();

}
