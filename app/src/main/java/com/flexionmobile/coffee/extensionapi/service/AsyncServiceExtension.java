package com.flexionmobile.coffee.extensionapi.service;

import android.content.Intent;

public interface AsyncServiceExtension extends ServiceExtension {

    Intent getActivityIntent();

    int getRequestCode();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
