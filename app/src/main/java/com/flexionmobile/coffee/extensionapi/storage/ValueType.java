package com.flexionmobile.coffee.extensionapi.storage;

public enum ValueType {
    INTEGER,
    STRING,
    BYTE_ARRAY

}
