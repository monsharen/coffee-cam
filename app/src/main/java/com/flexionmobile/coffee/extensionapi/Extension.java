package com.flexionmobile.coffee.extensionapi;

import android.content.Context;

import java.util.List;

public interface Extension {

    void initialise(Context context);

    void dispose();

    List<String> getPermissions();
}
