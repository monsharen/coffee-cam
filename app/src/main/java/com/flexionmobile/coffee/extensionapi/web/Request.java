package com.flexionmobile.coffee.extensionapi.web;

import java.util.Map;

public class Request {

    private final Map<String, String> urlParameters;

    public Request(Map<String, String> urlParameters) {
        this.urlParameters = urlParameters;
    }

    public String getParameter(String name) {
        return urlParameters.get(name);
    }
}
