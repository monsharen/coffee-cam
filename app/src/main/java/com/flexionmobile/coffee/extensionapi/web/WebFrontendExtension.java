package com.flexionmobile.coffee.extensionapi.web;

import android.text.Html;
import com.flexionmobile.coffee.extensionapi.Extension;

public interface WebFrontendExtension extends Extension {

    String getHeadElements();

    String getCss();

    String getBody();
}
