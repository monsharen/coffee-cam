package com.flexionmobile.coffee.extensionapi.storage;

public class StorageValue {

    private final String key;
    private final ValueType valueType;

    public StorageValue(String key, ValueType valueType) {
        this.key = key;
        this.valueType = valueType;
    }

    public String getKey() {
        return key;
    }

    public ValueType getValueType() {
        return valueType;
    }
}
