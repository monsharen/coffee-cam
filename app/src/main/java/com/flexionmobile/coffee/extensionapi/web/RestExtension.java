package com.flexionmobile.coffee.extensionapi.web;

import com.flexionmobile.coffee.extensionapi.Extension;
import org.json.JSONObject;

public interface RestExtension extends Extension {

    String getUri();

    JSONObject getResponse(Request request);
}
