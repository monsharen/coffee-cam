package com.flexionmobile.coffee.extensionapi.service;

import android.content.Context;
import android.content.Intent;
import com.flexionmobile.coffee.extensionapi.Extension;

public interface ServiceExtension extends Extension {

    Intent getServiceIntent(Context context);

}
