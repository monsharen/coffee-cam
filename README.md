# Flexion CoffeeCam #

An android app that periodically takes photos of coffee pots

![Screenshot](/screenshot.png?raw=true)

## About this project
- Simple to install. One Android APK that does it all
- Uses the Camera 2 API (which requires a Marshmallow device or above)
- Runs a WebServer which displays a webpage with up-to-date statistics
- Experimental feature: Gustav - The coffee pot recognition framework which analyses each image to see how much coffee is left in the pot

## Features to add ##
- Gustav - Add state analysis
- IFTTT integration
- Persist/load stats to/from private storage
- Auto update feature (periodically check for updated apk from external source)